<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property int $id
 * @property int $user_id
 * @property int $feed_id
 * @property-read User $user
 * @property-read Feed $feed
 */
class Subscription extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'feed_id',
    ];

    public function user(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function feed(): HasOne
    {
        return $this->hasOne(Feed::class, 'id', 'feed_id');
    }
}
