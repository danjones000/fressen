<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * @property int $id
 * @property string $name
 * @property string $url
 * @property string|null $site_url
 * @property Carbon $last_fetched
 */
class Feed extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'url',
        'site_url',
        'last_fetched',
    ];

    protected $casts = [
        'last_fetched' => 'datetime'
    ];
}
