# fReSSen Feed Reader

**Time to feed**

fReSSen is a feed reader built on a Laravel stack. It is a self-hosted solution.

It has an API which third-party clients can connect to. It's also compatible with the Tiny Tiny RSS API.

## What's with the name?

*fressen* is a German word, roughly translatable as "to feed", as in "the hungry wolf feeds on the helpless rabbit."

It's meant to be a bit of a play on words, in which the English meaning of *fressen** is the same word for an RSS feed, while also conveying a sense of devouring your content.

## What's done so far?

Well, barely anything really. But I'm working on it.

**You should not use this currently. I am making many breaking changes regularly. v0.5.0 will be safe to install without worrying about breaking changes, but you should probably hold off until v1.0.0.**

**Seriously. Don't use this yet. You won't be happy.**

## Roadmap

- [x] User creation
- [ ] API Working
- [ ] Add feeds from web
- [ ] Read feeds from web
- [ ] Create separate package that can be customized without modifying this code directly. Similar to the laravel/laravel and laravel/framework packages
